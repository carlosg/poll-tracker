# Polling script for tracker-miners configuration

This is a hacked up script to run polls related to configuration
changes in tracker-miners. To run:

```
$ ./poll-tracker.py
```

Or, if you have an unconventional location for tracker-miners (e.g. jhbuild):

```
$ ./poll-tracker.py /path/to/tracker-miner-fs-3
```

