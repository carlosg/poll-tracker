#!/bin/python3

from gi.repository import Gio, GLib
import os, re, shutil, subprocess, sys, time

SETTINGS_SCHEMA = 'org.freedesktop.Tracker3.Miner.Files'

RECURSIVE_HOMEDIR = {
    'index-recursive-directories': GLib.Variant.new_strv(['$HOME']),
    'index-single-directories': GLib.Variant.new_strv([])
}

FOLDER_COUNT_QUERY = """
select (CONCAT ('there are ', count(?folder), ' folders with more than ', ?cc, ' files')) {
{
  select ?folder (if (count(?o) >= 10000, 10000, if (count(?o) >= 1000, 1000, if (count(?o) >= 100, 100, if (count(?o) >= 10, 10, 1)))) AS ?cc) {
    ?folder a nfo:Folder . ?o nfo:belongsToContainer ?folder .
  } group by ?folder
}
} group by ?cc order by desc ?cc
"""

class replace_miner():
    def __init__(self, settings):
        self.settings = settings
        self.stored_settings = {}

        tracker3_cachedir = os.path.join(GLib.get_home_dir(), '.cache', 'tracker3')
        self.db_src = os.path.join(tracker3_cachedir, 'files')
        self.db_backup_dst = os.path.join(tracker3_cachedir, 'files.tracker-poll-backup')

    def __enter__(self):
        print('Stopping existing tracker-miner-fs-3 instance...')
        subprocess.run(['tracker3', 'daemon', '--kill'], stdout=subprocess.DEVNULL)

        print('Saving settings...')
        schema_source = Gio.SettingsSchemaSource.get_default()
        schema = schema_source.lookup(SETTINGS_SCHEMA, True)
        for s in schema.list_keys():
            if self.settings.get_value(s) != self.settings.get_default_value(s):
                self.stored_settings[s] = self.settings.get_user_value(s)
                self.settings.reset(s)
        self.settings.sync()

        print('Saving database...')
        shutil.move(self.db_src, self.db_backup_dst)

    def __exit__(self, etype, evalue, etraceback):
        print('Restoring database...')
        shutil.rmtree(self.db_src, ignore_errors=True)
        shutil.move(self.db_backup_dst, self.db_src)

        print('Restoring settings...')
        for s, v in self.stored_settings.items():
            if v != None:
                self.settings.set_value(s, v)
            else:
                self.settings.reset(s)
        self.settings.sync()

        print('Restarting tracker-miner-fs-3...')
        subprocess.run(['tracker3', 'daemon', '--start'], stdout=subprocess.DEVNULL)


class test():
    def __init__(self, settings, name, config):
        self.settings = settings
        self.config = config
        print('\n==================================')
        print('Testing %s...' % name)
        print('==================================\n')

        subprocess.run(['tracker3', 'reset', '--filesystem'], stdout=subprocess.DEVNULL)

    def __enter__(self):
        if self.config is not None:
            for s, v in self.config.items():
                self.settings.set_value(s, v)
        self.settings.sync()

    def __exit__(self, etype, evalue, etraceback):
        if self.config is not None:
            for s in self.config:
                self.settings.reset(s)
        self.settings.sync()


def run_test(exec_path):
    # First time index
    print('- From-scratch index (tracker-miner-fs-3):\t', end='', flush=True)
    start = time.monotonic()
    subprocess.run([exec_path, '-n', '-s', '0'], stderr=subprocess.DEVNULL)
    end = time.monotonic()
    cold = end - start
    print('%f seconds' % cold)

    # Second time index
    print ('- Hot cache restart (tracker-miner-fs-3):\t', end='', flush=True)
    start = time.monotonic()
    subprocess.run([exec_path, '-n', '-s', '0'], stderr=subprocess.DEVNULL)
    end = time.monotonic()
    hot = end - start
    print ('%f seconds' % hot)

    # Metadata extraction
    print ('- Metadata extraction (tracker-extract-3):\t', end='', flush=True)
    stats = b''
    status_output = b''
    env = os.environ
    env['TRACKER_DEBUG'] = 'statistics'
    start = time.monotonic()
    with subprocess.Popen([exec_path, '-s', '0'], env=env, stderr=subprocess.PIPE) as proc:
        output = b''
        statistics_found = False
        while not statistics_found or not output.endswith(b'--------------------------------------------------\n'):
            chunk = proc.stderr.readline()
            output += chunk
            if statistics_found is True:
                stats += chunk
            else:
                statistics_found = output.endswith(b'Statistics:\n')

        p = subprocess.run(['tracker3', 'status', '--stat'], capture_output = True)
        status_output = p.stdout

        p = subprocess.run(['tracker3', 'sparql', '-b', 'org.freedesktop.Tracker3.Miner.Files', '-q', FOLDER_COUNT_QUERY], capture_output = True)
        folder_stats_output = p.stdout

        proc.terminate()

    end = time.monotonic()
    # Discount 10s of tracker-extract-3 inactivity before shutdown,
    # and the hot start case which is also included in these timings.
    print ('%f seconds' % (end - start - 10 - hot))

    # Statistics
    pattern = re.compile(r'Tracker-Message:\s+[\S]+:\s+')
    print ('- Extractor statistics (TRACKER_DEBUG=statistics):\n\n%s\n' % pattern.subn('', stats.decode('utf-8'))[0])
    print ('- Total elements (tracker3 status --stat):\n\n%s\n' % status_output.decode('utf-8'))
    print ('- Folder stats. %s\n' % folder_stats_output.decode('utf-8'))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        miner_fs = sys.argv[1]
        if os.path.basename(miner_fs) != 'tracker-miner-fs-3':
            print ('Not a tracker-miner-fs-3 executable in argv[1]')
            sys.exit(-1)
    else:
        miner_fs = '/usr/libexec/tracker-miner-fs-3'
        if not os.path.exists(miner_fs):
            print ('%s is not found, specify a path to the executable in args' % miner_fs)
            sys.exit(-1)

    settings = Gio.Settings.new(SETTINGS_SCHEMA)
    with replace_miner(settings):
        try:
            with test(settings, 'default configuration', None):
                run_test(miner_fs)

            with test(settings, 'recursive homedir', RECURSIVE_HOMEDIR):
                run_test(miner_fs)
        except:
            print ('Something went wrong!')
